const path = require('path')

module.exports = {
  entry: path.resolve(__dirname, 'src', 'js', 'index.js'),
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader'
        }
      },
    ]
  },
}