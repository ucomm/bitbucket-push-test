const path = require('path')
const merge = require('webpack-merge')
const common = require('./webpack.common.js')

module.exports = merge(common, {
  mode: 'development',
  devtool: 'source-map',
  watch: true,
  output: {
    filename: 'index.js',
    path: path.resolve(__dirname, 'build', 'js', 'index.js'),
  },
})
